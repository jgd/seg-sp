/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.um.seguridad.sp.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.common.impl.RandomIdentifierGenerator;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author jgd
 */
@WebServlet(name = "ServiceProviderPrivateZoneHttpServlet", urlPatterns = {"/sp/private"})
public class ServiceProviderPrivateZoneHttpServlet extends HttpServlet {

    private final String URL_SP1 = "http://localhost:8080/seg-sp/sp/private";
    private final String IDENTITY_PROVIDER = "http://localhost:8080/seg-idp/idp";
    private final String URL_PRIVATE_ZONE = "http://localhost:8080/seg-sp/sp/private";
    private List<String> listaIDP;

    public ServiceProviderPrivateZoneHttpServlet() {
        //listaIDP.add("http://idp.um:8080");
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");

        try {


            PrintWriter out = response.getWriter();
            DefaultBootstrap.bootstrap();


           

            if (request.getSession().isNew( ) &&request.getSession().getAttribute("samlAssertions") == null && 
                   request.getParameter("responseSAML") ==null ){
                // Es la primera vez que venimos

                String respuesta = getAuthentication(URL_SP1, IDENTITY_PROVIDER, URL_PRIVATE_ZONE);

                /* TODO output your page here. You may use following sample code. */
                response.setContentType("text/html;charset=UTF-8");

                out.print("<!DOCTYPE html>");
                out.print("<html>");
                out.print("<head>");
                out.print("<title>PRIVATE</title>");
                out.print("</head>");
                out.print("<body>");
                out.print("<h1>ZONA PRIVADA</h1>");
                out.print("<p>Debe autenticarse</p>");
                out.print(respuesta);
                out.print("</body>");
                out.print("</html>");



            } else if (request.getSession().getAttribute("samlAssertions") != null) {
                // Ya estamos autenticados

                // Obtenemos el nombre del usuario
                Assertion assertion = (Assertion) request.getSession().getAttribute("samlAssertions");
                String name = assertion.getSubject().getNameID().getValue();

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>PRIVATE</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>ZONA PRIVADA</h1>");
                out.println("<p>Acceso Autorizado para el usuario </p>" + name);
                out.println("</body>");
                out.println("</html>");

            } else if(request.getParameter("responseSAML")!=null){
                // venimos redirigidos del IDP

                String responseString = request.getParameter("responseSAML").replace("'", "\"");
              //  System.out.println("ResponseString: " + responseString);

                // Tenemos que garantizar la entrada al usuario para futuras peticiones

                // Hay que comprobar la validez del mensaje SAML

                Response responseSAML = (Response) stringToSAML(responseString);
               
                //System.out.println("ID Response: " + responseSAML.getStatus().getStatusCode().getValue());
                if (!responseSAML.getStatus().getStatusCode().getValue().equals("urn:oasis:names:tc:SAML:2.0:status:Success")) {

                    // SAML no valido, se muestra pagina de aviso

                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>PRIVATE</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>ZONA PRIVADA</h1>");
                    out.println("<p>Acceso NO autorizado con  esa respuesta SAML");
                    out.println("</body>");
                    out.println("</html>");

                } else {

                    // El acceso esta autorizado y debemos incorporar el assertion a la sesion para futuras peticiones
                    request.getSession().setAttribute("samlAssertions", responseSAML.getAssertions().get(0));


                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>PRIVATE</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>ZONA PRIVADA</h1>");
                    out.println("<p>Acceso Autorizado. Hola por primera vez");
                    out.println("</body>");
                    out.println("</html>");

                }

            }

        } catch (ConfigurationException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLParserException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnmarshallingException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("rawtypes")
    private AuthnRequest createAuthNRequest(String issuerId, String destination, String responseURL) {
        // Create BuilderFactory
        XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
        // Create AuthnRequest
        SAMLObjectBuilder builder = (SAMLObjectBuilder) builderFactory.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
        AuthnRequest authnRequest = (AuthnRequest) builder.buildObject();
        authnRequest.setVersion(SAMLVersion.VERSION_20);
        authnRequest.setIssueInstant(new DateTime());
        authnRequest.setID(new RandomIdentifierGenerator().generateIdentifier());
        // Set Issuer
        builder = (SAMLObjectBuilder) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
        Issuer issuer = (Issuer) builder.buildObject();
        issuer.setFormat("urn:oasis:names:tc:SAML:2.0:nameid-format:entity");
        issuer.setValue(issuerId);
        authnRequest.setIssuer(issuer);
        // Set destination
        authnRequest.setDestination(destination);
        // Set response URL
        authnRequest.setAssertionConsumerServiceURL(responseURL);

        return authnRequest;
    }

    private String SAMLtoString(XMLObject object) throws MarshallingException {
        MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
        Marshaller marshaller = marshallerFactory.getMarshaller(object);
        org.w3c.dom.Element subjectElement = marshaller.marshall(object);
        return XMLHelper.prettyPrintXML(subjectElement);
    }

    private SAMLObject stringToSAML(String samlObject) throws UnsupportedEncodingException, XMLParserException, UnmarshallingException {
        System.out.print("stringSAMLSP: " + samlObject);

        InputStream is = new ByteArrayInputStream(samlObject.getBytes());
        BasicParserPool parser = new BasicParserPool();
        Document doc = parser.parse(is);
        Element samlElement = doc.getDocumentElement();
        UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
        Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(samlElement);
        return (SAMLObject) unmarshaller.unmarshall(samlElement);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getAuthentication(String urlSp, String urlIdp, String urlResource) throws UnsupportedEncodingException {


        // Genera la peticion de autenticacion y redirige al IDP
        AuthnRequest authnRequest = createAuthNRequest(urlSp, urlIdp, urlResource);
        String authnRequestString = "";
        String respuesta = "";
        try {
            authnRequestString = SAMLtoString(authnRequest);
            // System.out.println("SP " + authnRequestString);
            authnRequestString = authnRequestString.replaceAll("'", "\"");


            authnRequest = (AuthnRequest) stringToSAML(authnRequestString);
            System.out.println("ID: " + authnRequest.getID());
            authnRequestString = authnRequestString.replaceAll("\"", "'");
            //   System.out.println(authnRequestString);

        } catch (MarshallingException ex) {

            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLParserException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnmarshallingException ex) {
            Logger.getLogger(ServiceProviderPrivateZoneHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        respuesta = respuesta.concat("<form id=\"redirect\" action=\"" + IDENTITY_PROVIDER + "\" method=\"post\">");
        respuesta = respuesta.concat("<input type=\"hidden\" name=\"SAMLAuthRequest\" value=\"");
        respuesta = respuesta.concat(authnRequestString);
        respuesta = respuesta.concat("\"/><input type=\"submit\" value=\"\" style=\"display:hidden\">");
        respuesta = respuesta.concat("</form>");
        respuesta = respuesta.concat("<script> var form = document.getElementById('redirect'); form.submit(); </script>");

        return respuesta;

    }
}
